package junit;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class Junit4AssertionTest {
	@Test
	public void testAssert() {
		
		//Deklarasi variable
		String s1 = "Junit";
		String s2 = "Junit";
		String s3 = "Test";
		String s4 = "Test";
		String s5 = null;
		int variable1 = 1;
		int variable2 = 2;
		int[] airethematicArrary1 = {1,2,3};
		int[] airethematicArrary2 = {1,2,3};
		
		//Assert Statement
		assertEquals(s1, s2);
		assertSame(s3, s4);
		assertNotSame(s1, s3);
		assertNotNull(s1);
		assertNull(s5);
		assertTrue(variable1<variable2);
		assertArrayEquals(airethematicArrary1, airethematicArrary2);
	}

}
