package com.rndbe.haekal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HaekalApplication {

	public static void main(String[] args) {
		SpringApplication.run(HaekalApplication.class, args);
	}

}
